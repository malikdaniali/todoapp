class CreateTodos < ActiveRecord::Migration[5.0]
  def change
    create_table :todos do |t|
        t.string :name
        t.text :desc
    end
  end
end
