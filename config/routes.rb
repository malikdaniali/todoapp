Rails.application.routes.draw do

# Home route
root "pages#home"

#About route
get '/about', to: "pages#about"

#Help route
get '/help', to: "pages#help"

resources :todos
# For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
